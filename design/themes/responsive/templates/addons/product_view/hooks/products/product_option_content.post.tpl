<table border="4" style="width: 100px; lenght: 100px;">
<tr>
<td><center><span class="ty-control-group__label product-list-field">Цена:</span></center></td>
<td width='200px' nowrap><center><span class="ty-control-group__label product-list-field">Опции товара:</span></center></td>
<td><center><label class="ty-control-group__label">КОД:</label></center></td>
<td><center><label class="ty-control-group__label">Доступность:</label></center></td>
<td><center><label class="ty-control-group__label">Кол-во:</label></center></td>
<td><center><label class="ty-control-group__label">Заказ:</label></center></td>
</tr>
<tr>
<td><center>{if $product.price|floatval || $product.zero_price_action == "P" || ($hide_add_to_cart_button == "Y" && $product.zero_price_action == "A")}
                    <span class="ty-price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{include file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="ty-price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>
                {elseif $product.zero_price_action == "A" && $show_add_to_cart}
                    {assign var="base_currency" value=$currencies[$smarty.const.CART_PRIMARY_CURRENCY]}
                    <span class="ty-price-curency"><span class="ty-price-curency__title">{__("enter_your_price")}:</span>
                    <div class="ty-price-curency-input">
                        {if $base_currency.after != "Y"}{$base_currency.symbol nofilter}{/if}
                        <input class="ty-price-curency__input" type="text" size="3" name="product_data[{$obj_id}][price]" value="" />
                        {if $base_currency.after == "Y"}{$base_currency.symbol nofilter}{/if}
                    </div>
                    </span>

                {elseif $product.zero_price_action == "R"}
                    <span class="ty-no-price">{__("contact_us_for_price")}</span>
                    {assign var="show_qty" value=false}
                {/if}</center></td>
                    
    <td width='300px' nowrap align="left">       
            {include file="addons/product_view/views/product_view/product_options.tpl" id=$obj_id product_options=$product.product_options name="product_data" capture_options_vs_qty=$capture_options_vs_qty disable_ids=$_disable_ids}
    </td>
<td><center>      
        <div class="ty-control-group ty-sku-item cm-reload-{$obj_prefix}{$obj_id}{if !$product.product_code} hidden{/if}" id="sku_update_{$obj_prefix}{$obj_id}">
            <input type="hidden" name="appearance[show_sku]" value="{$show_sku}" />
            <label class="ty-control-group__label" id="sku_{$obj_prefix}{$obj_id}">{__("")}</label>
            <span class="ty-control-group__item" id="product_code_{$obj_prefix}{$obj_id}">{$product.product_code}</span>
        <!--sku_update_{$obj_prefix}{$obj_id}--></div>
   </center></td>
<td><center><span class="ty-qty-in-stock ty-control-group__item" id="in_stock_info_127">В наличии</span></center></td>
<td><center><div class="ty-center ty-value-changer cm-value-changer">
        <a class="cm-increase ty-value-changer__increase">+</a>
        <input type="text" size="5" class="ty-value-changer__input cm-amount" id="qty_count_127" name="product_data[127][amount]" value="1" data-ca-min-qty="1">
        <a class="cm-decrease ty-value-changer__decrease">-</a>
    </div></center>
</td>
<td><center><div class="ty-product-list__control">
        {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
        {$smarty.capture.$add_to_cart nofilter}
    </div></center></td>
</tr>
</table> 
